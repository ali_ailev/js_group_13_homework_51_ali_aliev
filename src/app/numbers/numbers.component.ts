import { Component } from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent{
  numbers: number[] = [];
  showLoto = false;

  getNumbers () {
    const array: number[] = []
    this.numbers = [];
    let index = 0;

    while (index !== 5) {
      const number = (Math.round(Math.random() * (32 - 5) + 5));
      let flag = true;
      for (let i = 0 ; i < array.length ; i++) {
        if (number === array[i]) {
          flag = false;
          continue;
        } else {
          flag = true;
        }
      }
      if (flag) {
        array.push(number);
        index++;
      }
    }
    this.numbers = array.sort((a, b) => {
      return a - b;
    });
  }
}
