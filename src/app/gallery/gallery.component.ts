import { Component } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  title = '';
  pic = '';
  passwordInput = '';
  password = '';
  images = [
    ['https://statusas.ru/wp-content/uploads/2020/06/statusas-auf-volk3.jpg','Wolf'],
    ['https://scontent.ffru7-1.fna.fbcdn.net/v/t1.6435-9/128426350_3373666256016253_1642967249958463302_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=36a2c1&_nc_ohc=6z-JD4t_uUYAX_heSIF&_nc_ht=scontent.ffru7-1.fna&oh=3f46cdbab7cd8d6f3294cc527c37dc12&oe=61B4AA27','Waterfall'],
    ['https://sun3-10.userapi.com/s/v1/if1/y72Ov_XZPLyuIeAQsCKjkYj31CV4h3ULfIBf755hFxDLRpqvvyxv0Rcs-RDqK0dOBufGqVX5.jpg?size=200x235&quality=96&crop=0,0,500,588&ava=1', 'Crige']];

  checkPassword () {
    return this.password === 'limon123'
  }

  formIsEmpty () {
    return this.title === '' || this.pic === ''
  }

  addImage (event: Event) {
    event.preventDefault();
    const imagesInner: string[] = [];
    imagesInner.push(this.pic,this.title);
    this.images.push(imagesInner);
  }
}
